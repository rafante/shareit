part of share_it;

@visibleForTesting
class ShareItConstants {
  static const methodChannel = 'share_it';
  static const iOSInvokeShareMethod = 'shareItiOS';
  static const androidInvokeShareMethod = 'shareItAndroid';
}
