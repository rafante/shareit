import Flutter
import UIKit

struct ShareItParameters: Decodable {
	enum ShareItType: String, Decodable {
		case text, image, uiImage, file, link
	}
	let content: String
	let type: ShareItType
}

public class SwiftShareItPlugin: NSObject, FlutterPlugin {

	public static func register(with registrar: FlutterPluginRegistrar) {
		let channel = FlutterMethodChannel(name: "share_it", binaryMessenger: registrar.messenger())
		let instance = SwiftShareItPlugin()
		registrar.addMethodCallDelegate(instance, channel: channel)
	}

	public func handle(_ call: FlutterMethodCall, result: @escaping FlutterResult) {
		result(true)
		DispatchQueue.global().async {
			if call.method == "shareItiOS" {
				guard let arguments = call.arguments else {
					NSLog("Empty arguments")
					return
				}

				guard let shareItiOSData = try? JSONSerialization.data(withJSONObject: arguments, options: []) else {
					NSLog("Invalid arguments")
					return
				}
				guard let parameters = try? JSONDecoder().decode(Array<ShareItParameters>.self, from: shareItiOSData) else {
					NSLog("Error decoding arguments")
					return
				}

				DispatchQueue.main.async {
					self.activityController(parameters: parameters)
				}
			}
		}
	}

	// private

	private func activityController(parameters: [ShareItParameters], completionHandler: (() -> Void)? = nil) {
		let activityVC = UIActivityViewController(activityItems: activityItems(for: parameters), applicationActivities: nil)
		activityVC.popoverPresentationController?.sourceView = rootVC?.view
		if let view = rootVC?.view {
			activityVC.popoverPresentationController?.permittedArrowDirections = []
			activityVC.popoverPresentationController?.sourceRect = CGRect(x: view.bounds.midX, y: view.bounds.midY, width: 0, height: 0)
		}
		rootVC?.present(activityVC, animated: true, completion: completionHandler)
	}

	private var rootVC: UIViewController? {
		if #available(iOS 13, *) {
			return UIApplication.shared.connectedScenes
				.filter({$0.activationState == .foregroundActive})
				.map({$0 as? UIWindowScene})
				.compactMap({$0})
				.first?.windows
				.filter({$0.isKeyWindow}).first?.rootViewController
		}
		return UIApplication.shared.keyWindow?.rootViewController
	}

	private func activityItems(for parameters: [ShareItParameters]) -> [Any] {
		var activityParameters: [Any] = []
		for parameter in parameters {
			switch parameter.type {
			case .text:
				activityParameters.append(parameter.content)
			case .uiImage:
				if !FileManager.default.fileExists(atPath: parameter.content) {
					let url = URL(fileURLWithPath: parameter.content)
					guard let data = try? Data(contentsOf: url) else { return [] }
					guard let image = UIImage(data: data) else { return [] }
					activityParameters.append(image)
				} else {
					activityParameters.append(parameter.content)
				}
			case .link:
				let url = URL(string: parameter.content)
				let urlOrPlainContent: Any = url ?? parameter.content
				activityParameters.append(urlOrPlainContent)
			case .image, .file:
				guard FileManager.default.fileExists(atPath: parameter.content) else {
					return [parameter.content]
				}
				let url = URL(fileURLWithPath: parameter.content)
				activityParameters.append(url)
			}
		}
		return activityParameters
	}

}
