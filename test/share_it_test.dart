import 'dart:collection';

import 'package:flutter/services.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:share_it/share_it.dart';

void main() {
  const MethodChannel channel = MethodChannel(ShareItConstants.methodChannel);

  setUp(() {
    channel.setMockMethodCallHandler((MethodCall methodCall) async {
      expect(methodCall.arguments is List, true);
      switch (methodCall.method) {
        case ShareItConstants.iOSInvokeShareMethod:
          final argumentsList = methodCall.arguments as List;
          for (final arguments_ in argumentsList) {
            final arguments = arguments_ as LinkedHashMap<dynamic, dynamic>;
            expect(arguments['type'] != null, true);
            expect(arguments['content'] != null, true);

            if (arguments['type'] == IOSDataType.plainText.rawValue) {
              expect(arguments['type'], IOSDataType.plainText.rawValue);
              expect(arguments['content'], 'something');
            } else {
              expect(arguments['type'], IOSDataType.file.rawValue);
              expect(arguments['content'], 'somePath');
            }
          }
          return true;
        case ShareItConstants.androidInvokeShareMethod:
          final argumentsList = methodCall.arguments as List;
          for (final arguments_ in argumentsList) {
            final arguments = arguments_ as LinkedHashMap<dynamic, dynamic>;
            expect(arguments['type'] != null, true);
            expect(arguments['content'] != null, true);
            expect(arguments['title'] != null, true);
            expect(arguments['shareAsPlainText'] != null, true);

            expect(arguments['shareAsPlainText'] == true || arguments['shareAsPlainText'] == false, true);

            expect(arguments['title'], 'a title');

            if (arguments['shareAsPlainText'] == true) {
              expect(arguments['type'], MIMEType.plainText.rawValue);
              expect(arguments['content'], 'something2');
            } else {
              expect(arguments['type'], MIMEType.image.rawValue);
              expect(arguments['content'], 'somePath2');
            }
          }
          return true;
        default:
          fail("Incorrect method");
      }
    });
  });

  tearDown(() {
    channel.setMockMethodCallHandler(null);
  });

  test(ShareItConstants.iOSInvokeShareMethod, () async {
    expect(await ShareIt.iOSText(content: 'something'), true);
  });

  test(ShareItConstants.androidInvokeShareMethod, () async {
    expect(await ShareIt.androidText(content: 'something2', title: 'a title'), true);
  });

  test('${ShareItConstants.iOSInvokeShareMethod}file', () async {
    expect(await ShareIt.iOSFile(dataType: IOSDataType.file, path: 'somePath'), true);
  });

  test('${ShareItConstants.androidInvokeShareMethod}file', () async {
    expect(await ShareIt.androidFile(mimeType: MIMEType.image, path: 'somePath2', title: 'a title'), true);
  });
}
