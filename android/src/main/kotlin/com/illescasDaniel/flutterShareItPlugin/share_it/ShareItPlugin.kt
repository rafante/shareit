package com.illescasDaniel.flutterShareItPlugin.share_it

import androidx.annotation.NonNull
import android.content.Context
import android.content.Intent
import android.net.Uri
import android.os.Build
import androidx.core.content.FileProvider
import io.flutter.embedding.engine.plugins.FlutterPlugin
import io.flutter.plugin.common.MethodCall
import io.flutter.plugin.common.MethodChannel
import io.flutter.plugin.common.MethodChannel.MethodCallHandler
import io.flutter.plugin.common.MethodChannel.Result
import io.flutter.plugin.common.PluginRegistry.Registrar
import java.io.File
import java.lang.Exception

data class ShareItParameters(val map: Map<String, Any?>) {
  val type: String by map
  val title: String? by map
  val content: String by map
  val shareAsPlainText: Boolean by map
}

class ShareItPlugin: FlutterPlugin, MethodCallHandler {

  /// The MethodChannel that will the communication between Flutter and native Android
  ///
  /// This local reference serves to register the plugin with the Flutter Engine and unregister it
  /// when the Flutter Engine is detached from the Activity
  private lateinit var channel: MethodChannel
  private lateinit var context: Context

  override fun onAttachedToEngine(@NonNull flutterPluginBinding: FlutterPlugin.FlutterPluginBinding) {
    channel = MethodChannel(flutterPluginBinding.binaryMessenger, "share_it")
    channel.setMethodCallHandler(this)
    context = flutterPluginBinding.applicationContext
  }

  override fun onMethodCall(@NonNull call: MethodCall, @NonNull result: Result) {
    when (call.method) {
      "shareItAndroid" -> {
        val argumentsList = call.arguments
        if (argumentsList != null && argumentsList is List<*>) {
          val mappedParameters: MutableList<ShareItParameters> = mutableListOf()
          for (arguments in argumentsList) {
            if (arguments != null && arguments is Map<*,*>) {
              val parameters = ShareItParameters(map = mapOf(
                "type" to (arguments["type"] ?: "*/*"),
                "title" to arguments["title"],
                "content" to (arguments["content"] ?: ""),
                "shareAsPlainText" to (arguments["shareAsPlainText"] ?: false)
              ))
              mappedParameters.add(parameters);
            } else {
              result.error("Incorrect arguments", null, null)
            }
          }
          result.success(true)

          if (mappedParameters.size == 1) {
            shareContent(mappedParameters.first())
          } else {
            shareMultipleContent(mappedParameters)
          }
        }
      }
      else -> result.notImplemented()
    }
  }

  override fun onDetachedFromEngine(@NonNull binding: FlutterPlugin.FlutterPluginBinding) {
    channel.setMethodCallHandler(null)
  }

  private fun shareContent(parameters: ShareItParameters) {
    val context = getActiveContext()
    val shareIntent = Intent().apply {
      action = Intent.ACTION_SEND
      if (parameters.type == "text/plain" && parameters.shareAsPlainText) {
        putExtra(Intent.EXTRA_TEXT, parameters.content)
      } else {
        val uri = uriForPath(parameters.content)
        putExtra(Intent.EXTRA_STREAM, uri)
      }
      type = parameters.type
    }
    val title: CharSequence? = parameters.title

    val chooserIntent = Intent.createChooser(shareIntent, title)
    chooserIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
    context.startActivity(chooserIntent)
  }

  private fun shareMultipleContent(parametersList: List<ShareItParameters>) {
    val context = getActiveContext()
    var title: CharSequence? = null
    val shareIntent = Intent().apply {

      action = Intent.ACTION_SEND_MULTIPLE

      val uris = arrayListOf<Uri>()

      for (parameters in parametersList) {
        if (parameters.type == "text/plain" && parameters.shareAsPlainText) {
          putStringArrayListExtra(Intent.EXTRA_TEXT, arrayListOf(parameters.content))
          try {
            // android outputs a cast exception about this, but it's the only way I found of doing it
            // that it truly works, because the above doesn't do what I want for emails or the
            // messages app AFAIK
            putExtra(Intent.EXTRA_TEXT, parameters.content)
          } catch (e: Error) {}
        } else {
          uris.add(uriForPath(parameters.content))
        }
        parameters.title?.let { title = it }
      }
      putParcelableArrayListExtra(Intent.EXTRA_STREAM, uris)
      type = "*/*"
    }

    val chooserIntent = Intent.createChooser(shareIntent, title)
    chooserIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
    context.startActivity(chooserIntent)
  }

  private fun uriForPath(path: String): Uri {

    val context = getActiveContext()

    val file = File(path)
    require(file.exists()) { "File doesn't exist" }

    val authorities = context.packageName + ".shareit.fileprovider"

    val versionBelowAndroidN = Build.VERSION.SDK_INT < Build.VERSION_CODES.N
    return if (versionBelowAndroidN) Uri.fromFile(file)
            else FileProvider.getUriForFile(context, authorities, file)
  }

  private fun getActiveContext(): Context {
    return this.context
  }
}
